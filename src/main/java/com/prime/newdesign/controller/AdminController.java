package com.prime.newdesign.controller;

import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.prime.newdesign.model.Effect;
import com.prime.newdesign.model.Factory;
import com.prime.newdesign.model.Material;
import com.prime.newdesign.model.Product;
import com.prime.newdesign.model.Size;
import com.prime.newdesign.model.Technology;
import com.prime.newdesign.service.EffectService;
import com.prime.newdesign.service.FactoryService;
import com.prime.newdesign.service.MaterialService;
import com.prime.newdesign.service.ProductService;
import com.prime.newdesign.service.SizeService;
import com.prime.newdesign.service.TechnologyService;
import com.prime.newdesign.util.Constants;


@Controller
public class AdminController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SizeService sizeService;
	
	@Autowired
	FactoryService factoryService;
	
	@Autowired
	TechnologyService technologyService;
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	EffectService effectService;
	
	@Autowired
	Environment env;
	
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(Model model) {
		return "admin";
	}
	
	@RequestMapping(value = "/admin/upload", method = RequestMethod.POST)
	public String uploadImage(
			@RequestParam(name = "code", required = false) String code,
			@RequestParam("fileimage") MultipartFile fileimage, Model model,
			HttpServletRequest request) {
		String imageFront = fileimage.getOriginalFilename();
		if(!code.isEmpty()){
			imageFront = code.concat(Constants.DOT).concat(Constants.JPEG);
		}
		uploadGallery(request, fileimage, imageFront);
		return "admin";
	}
	
	@RequestMapping(value = "/admin/import", method = RequestMethod.POST)
	public String importdata(@RequestParam("file") MultipartFile file,Model model,HttpServletRequest request) {
		List<Product> products = new ArrayList<Product>();
		Set<String> sizes = new HashSet<String>();
		Set<String> factories = new HashSet<String>();
		Set<String> techonologies = new HashSet<String>();
		Set<String> materials = new HashSet<String>();
		Set<String> effects = new HashSet<String>();
		try {
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				if(rowindex == 0){
					continue;
				}
				Product product = new Product();
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							String factory = convertFactory(cellValue);
							factories.add(factory);
							product.setFactory(factory);
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setLaborcode(cellValue);
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							sizes.add(cellValue);
							product.setSize(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							sizes.add(cellValue);
							product.setSize(cellValue);
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							techonologies.add(cellValue);
							product.setTechnology(cellValue);
						}
						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							materials.add(cellValue);
							product.setMaterial(cellValue);
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							effects.add(cellValue);
							product.setEffect(cellValue);
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							if(cellValue != null && !cellValue.isEmpty()){
								Date selectedDate = df.parse(cellValue);
								product.setSelecteddate(selectedDate);
							}
						}
						break;
					case 7:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							if(cellValue != null && !cellValue.isEmpty()){
								Date productionDate = df.parse(cellValue);
								product.setProductiondate(productionDate);
							}
						}
						break;
					case 8:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setCode(cellValue);
						}
						break;
					case 9:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setDescription(cellValue);
						}
						break;
					default:
						break;
					}
				}
				products.add(product);
			}
			// save product
			for (Product product : products) {
				String code = product.getCode();
				Product existProduct = productService.findByCode(code);
				if(existProduct == null){
					productService.saveProduct(product);
				}
			}
			
			//save factory
			for (String factoryName : factories) {
				Factory existFactory = factoryService.findByName(factoryName);
				if(existFactory == null){
					Factory newFactory = new Factory();
					newFactory.setName(factoryName);
					factoryService.saveFact(newFactory);
				}
			}
			//save size
			for (String sizeName : sizes) {
				Size existSize = sizeService.findBySize(sizeName);
				if(existSize == null){
					Size newSize = new Size();
					newSize.setSize(sizeName);
					sizeService.saveSize(newSize);
				}
			}
			
			//save technology
			for (String technologyName : techonologies) {
				Technology existTech = technologyService.findByTechnology(technologyName);
				if(existTech == null){
					Technology newTech = new Technology();
					newTech.setTechnology(technologyName);
					technologyService.saveTechnology(newTech);
				}
			}
			//save material
			for (String materialname : materials) {
				Material existMaterial = materialService.findMaterial(materialname);
				if(existMaterial == null){
					Material newMaterial = new Material();
					newMaterial.setMaterial(materialname);
					materialService.saveMaterial(newMaterial);
				}
			}
			//save effect
			for (String effectName : effects) {
				Effect existEffect = effectService.findByEffect(effectName);
				if(existEffect == null){
					Effect newEffect = new Effect();
					newEffect.setEffect(effectName);
					effectService.saveEffect(newEffect);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "admin";
	}
	
	private void uploadGallery(HttpServletRequest request,MultipartFile image, String fileName){
//		String path = "D:\\Source\\nd\\newdesign\\src\\main\\resources\\static\\assets\\tiles";
		String path = request.getServletContext().getRealPath("")
				.concat(File.separator).concat("WEB-INF")
				.concat(File.separator).concat("classes")
				.concat(File.separator).concat("static")
				.concat(File.separator).concat("assets")
				.concat(File.separator).concat("tiles");
		File fileFolder = new File(path);
		System.out.println("path: "+ path);
    	if(!fileFolder.exists()){
    		fileFolder.mkdir();
    	}
    	int indexDot = fileName.lastIndexOf(Constants.DOT);
    	String orgFileName = fileName.substring(0, indexDot).concat(Constants.DOT).concat(Constants.JPEG);
    	path = path.concat(File.separator).concat(orgFileName);
    	File fileImageSave = new File(path);
    	System.out.println("image path: "+ path);
    	if(fileImageSave.exists()){
    		fileImageSave.delete();
    	}
    	try {
    		image.transferTo(fileImageSave);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String convertFactory(String cellValue){
		String factory = Constants.EMPTY;
		switch (cellValue) {
		case Constants.DL:
			factory = Constants.DAI_LOC;
			break;
		case Constants.PY:
			factory = Constants.PHO_YEN;
			break;
		case Constants.YB:
			factory = Constants.YEN_BINH;
			break;
		case Constants.DV:
			factory = Constants.DAI_VIET;
			break;
		case Constants.TP:
			factory = Constants.TIEN_PHONG;
			break;
		case Constants.VP:
			factory = Constants.VINH_PHUC;
			break;
		default:
			break;
		}
		return factory;
	}
	
}
