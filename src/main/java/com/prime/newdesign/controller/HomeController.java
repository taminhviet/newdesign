package com.prime.newdesign.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.newdesign.model.Material;
import com.prime.newdesign.model.Product;
import com.prime.newdesign.model.Size;
import com.prime.newdesign.model.Technology;
import com.prime.newdesign.model.User;
import com.prime.newdesign.service.FactoryService;
import com.prime.newdesign.service.MaterialService;
import com.prime.newdesign.service.ProductService;
import com.prime.newdesign.service.SizeService;
import com.prime.newdesign.service.TechnologyService;
import com.prime.newdesign.service.UserService;
import com.prime.newdesign.util.Constants;
import com.prime.newdesign.model.Factory;

@Controller
public class HomeController {

	@Autowired
	UserService userService;
	
	@Autowired
	FactoryService factoryService;
	
	@Autowired
	SizeService sizeService;
	
	@Autowired
	TechnologyService technologyService;
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Environment env;
	
	@RequestMapping(value = "/designer", method = RequestMethod.GET)
	public String search(Model model) {
		//filter fact
		List<String> filterFactory = new ArrayList<String>();
		filterFactory.add("-- Nhà máy --");
		List<Factory> lsFactory = factoryService.getAllFact();
		for (Factory factory : lsFactory) {
			filterFactory.add(factory.getName());
		}
		model.addAttribute("filterFactory", filterFactory);
		
		//filter size
		List<String> filterSize = new ArrayList<String>();
		filterSize.add("-- Kích cỡ --");
		List<Size> lsSize =  sizeService.getAllSize();
		for (Size size : lsSize) {
			filterSize.add(size.getSize());
		}
		model.addAttribute("filterSize", filterSize);
		
		//filter technology
		List<String> filterTechnology = new ArrayList<String>();
		filterTechnology.add("-- Công nghệ -- ");
		List<Technology> lsTechnology = technologyService.getAll();
		for (Technology technology : lsTechnology) {
			filterTechnology.add(technology.getTechnology());
		}
		model.addAttribute("filterTechnology", filterTechnology);
		
		//filter material
		List<String> filterMaterial = new ArrayList<String>();
		filterMaterial.add("-- Chất liệu --");
		List<Material> lsMaterial = materialService.findAll();
		for (Material material : lsMaterial) {
			filterMaterial.add(material.getMaterial());
		}
		model.addAttribute("filterMaterial", filterMaterial);
		return "search";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		if (currentPrincipalName.equalsIgnoreCase("anonymousUser")) {
			return "accessdenied";
		}
		User user = userService.findbyUsername(currentPrincipalName);
		if (Constants.ROLE_ADMIN == user.getIsAdmin()) {
			return "redirect:/admin";
		} else {
			return "redirect:/designer";
		}
	}
	
	@RequestMapping(value = "/designer/search", method = RequestMethod.POST)
	public String searchdata(Model model,
			@RequestParam(name = "fromdate", required = false) String fromdate,
			@RequestParam(name = "todate", required = false) String todate,
			@RequestParam(name = "factory", required = false) String factoryfilter,
			@RequestParam(name = "size", required = false) String sizefilter,
			@RequestParam(name = "technology", required = false) String technologyfilter,
			@RequestParam(name = "material", required = false) String materialfilter,
			@RequestParam(name = "description", required = false) String descriptionfilter) {
		// filter fact
		List<String> filterFactory = new ArrayList<String>();
		filterFactory.add("-- Nhà máy --");
		List<Factory> lsFactory = factoryService.getAllFact();
		for (Factory factory : lsFactory) {
			filterFactory.add(factory.getName());
		}
		model.addAttribute("filterFactory", filterFactory);

		// filter size
		List<String> filterSize = new ArrayList<String>();
		filterSize.add("-- Kích cỡ --");
		List<Size> lsSize = sizeService.getAllSize();
		for (Size size : lsSize) {
			filterSize.add(size.getSize());
		}
		model.addAttribute("filterSize", filterSize);

		// filter technology
		List<String> filterTechnology = new ArrayList<String>();
		filterTechnology.add("-- Công nghệ -- ");
		List<Technology> lsTechnology = technologyService.getAll();
		for (Technology technology : lsTechnology) {
			filterTechnology.add(technology.getTechnology());
		}
		model.addAttribute("filterTechnology", filterTechnology);

		// filter material
		List<String> filterMaterial = new ArrayList<String>();
		filterMaterial.add("-- Chất liệu --");
		List<Material> lsMaterial = materialService.findAll();
		for (Material material : lsMaterial) {
			filterMaterial.add(material.getMaterial());
		}
		model.addAttribute("filterMaterial", filterMaterial);
		List<Product> lsProducts = new ArrayList<Product>();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if(!fromdate.isEmpty() && !todate.isEmpty()){
				Date fromDateFilter = df.parse(fromdate);
				Date toDateFilter = df.parse(todate);
				lsProducts = productService.searchByTime(toDateFilter, fromDateFilter);
			}else {
				//Get all product
				lsProducts = productService.getAll();
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//factory search
		if(!factoryfilter.contains(Constants.MINUS)){
			for (Iterator iterator = lsProducts.iterator(); iterator.hasNext();) {
				Product product = (Product) iterator.next();
				if(!product.getFactory().equalsIgnoreCase(factoryfilter)){
					iterator.remove();
				}
			}
		}
		//size search
		if(!sizefilter.contains(Constants.MINUS)){
			for (Iterator iterator = lsProducts.iterator(); iterator.hasNext();) {
				Product product = (Product) iterator.next();
				if(!product.getSize().equalsIgnoreCase(sizefilter)){
					iterator.remove();
				}
			}
		}
		//technology search
		if(!technologyfilter.contains(Constants.MINUS)){
			for (Iterator iterator = lsProducts.iterator(); iterator.hasNext();) {
				Product product = (Product) iterator.next();
				if(!product.getTechnology().equalsIgnoreCase(technologyfilter)){
					iterator.remove();
				}
			}
		}
		//material search
		if(!materialfilter.contains(Constants.MINUS)){
			for (Iterator iterator = lsProducts.iterator(); iterator.hasNext();) {
				Product product = (Product) iterator.next();
				if(!product.getMaterial().equalsIgnoreCase(materialfilter)){
					iterator.remove();
				}
			}
		}
		//description search
		if(!descriptionfilter.isEmpty()){
			for (Iterator iterator = lsProducts.iterator(); iterator.hasNext();) {
				Product product = (Product) iterator.next();
				if(!product.getDescription().contains(descriptionfilter)){
					iterator.remove();
				}
			}
		}
		for (Product product : lsProducts) {
			String code = product.getCode();
			if(code.contains("\"")){
				code = code.replaceAll("\"", Constants.EMPTY);
			}
			String pathImage = code.concat(".jpeg");
			if(pathImage.contains("\"")){
				pathImage = pathImage.replaceAll("\"", Constants.EMPTY);
			}
			product.setUrl(pathImage);
		}
		model.addAttribute("lsProducts", lsProducts);
		return "search";

	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(Model model,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam(name = "phone", required = false) String phone,
			@RequestParam(name = "fullname", required = false) String fullname,
			@RequestParam(name = "address", required = false) String address) {
		User user = new User();
		if (username.contains("admin")) {
			user.setIsAdmin(Constants.ROLE_ADMIN);
		}else {
			user.setIsAdmin(Constants.ROLE_DESIGNER);
		}
		user.setUsername(username);
		user.setPassword(password);
		user.setPhone(phone);
		user.setFullname(fullname);
		user.setAddress(address);
		userService.saveUser(user);
		return "login";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {
		return "register";
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error(Model model) {
		return "error";
	}

	@RequestMapping(value = "/access-denied", method = RequestMethod.GET)
	public String access(Model model) {
		return "accessdenied";
	}
}
