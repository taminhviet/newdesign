package com.prime.newdesign.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.newdesign.model.Effect;

@Repository
public interface EffectRepository extends JpaRepository<Effect, Integer>{
	
	Effect findByEffect(String effect);
}
