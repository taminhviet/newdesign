package com.prime.newdesign.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prime.newdesign.model.Material;

public interface MaterialRepository extends JpaRepository<Material, Integer>{

	Material findByMaterial(String material);
}
