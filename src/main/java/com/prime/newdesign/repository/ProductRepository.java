package com.prime.newdesign.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prime.newdesign.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

	Product findByCode(String code);
	
	
	@Transactional
	@Query("SELECT p from Product p WHERE p.selecteddate <= :todate AND p.selecteddate >= :fromdate")
	List<Product> searchByTime(@Param("todate") Date todate,@Param("fromdate") Date fromdate);
}
