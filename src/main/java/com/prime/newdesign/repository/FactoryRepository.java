package com.prime.newdesign.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.newdesign.model.Factory;

@Repository
public interface FactoryRepository extends JpaRepository<Factory, Integer>{

	Factory findById(String id);
	Factory findByName(String name);
}
