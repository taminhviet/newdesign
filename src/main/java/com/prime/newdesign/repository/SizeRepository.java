package com.prime.newdesign.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.newdesign.model.Size;

@Repository
public interface SizeRepository extends JpaRepository<Size, Integer>{

	Size findBySize(String size);
}
