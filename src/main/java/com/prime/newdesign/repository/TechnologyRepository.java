package com.prime.newdesign.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prime.newdesign.model.Technology;

public interface TechnologyRepository extends JpaRepository<Technology, Integer>{

	Technology findByTechnology(String technology);
}
