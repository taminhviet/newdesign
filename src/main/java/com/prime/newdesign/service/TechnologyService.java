package com.prime.newdesign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.Technology;
import com.prime.newdesign.repository.TechnologyRepository;

@Service
public class TechnologyService {

	@Autowired
	TechnologyRepository technologyRepository;
	
	public List<Technology> getAll(){
		return technologyRepository.findAll();
	}
	public void saveTechnology(Technology technology){
		technologyRepository.save(technology);
	}
	public Technology findByTechnology(String technology){
		return technologyRepository.findByTechnology(technology);
	}
}
