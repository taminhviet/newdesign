package com.prime.newdesign.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.Product;
import com.prime.newdesign.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	public void saveProduct(Product product){
		productRepository.save(product);
	}
	public List<Product> getAll(){
		return productRepository.findAll();
	}
	
	public Product findByCode(String code){
		return productRepository.findByCode(code);
	}
	
	public List<Product> searchByTime(Date todate, Date fromDate){
		return productRepository.searchByTime(todate, fromDate);
	}
}
