package com.prime.newdesign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.Factory;
import com.prime.newdesign.repository.FactoryRepository;

@Service
public class FactoryService {

	@Autowired
	FactoryRepository factoryReposity;
	
	public List<Factory> getAllFact(){
		return factoryReposity.findAll();
	}
	
	public void saveFact(Factory factory){
		factoryReposity.save(factory);
	}
	
	public Factory findById(String id){
		return factoryReposity.findById(id);
	}
	
	public Factory findByName(String name){
		return factoryReposity.findByName(name);
	}
	
}
