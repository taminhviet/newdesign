package com.prime.newdesign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.Material;
import com.prime.newdesign.repository.MaterialRepository;

@Service
public class MaterialService {
	
	@Autowired
	MaterialRepository materialRepository;

	public List<Material> findAll(){
		return materialRepository.findAll();
	}
	
	public void saveMaterial(Material material){
		materialRepository.save(material);
	}
	
	public Material findMaterial(String material){
		return materialRepository.findByMaterial(material);
	}
}
