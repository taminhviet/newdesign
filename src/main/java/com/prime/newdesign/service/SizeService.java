package com.prime.newdesign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.Size;
import com.prime.newdesign.repository.SizeRepository;

@Service
public class SizeService {

	@Autowired
	SizeRepository sizeReposity;
	
	public List<Size> getAllSize(){
		return sizeReposity.findAll();
	}
	
	public void saveSize(Size size){
		sizeReposity.save(size);
	}
	
	public Size findBySize(String size){
		return sizeReposity.findBySize(size);
	}
}
