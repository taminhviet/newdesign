package com.prime.newdesign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.Effect;
import com.prime.newdesign.repository.EffectRepository;

@Service
public class EffectService {
	
	@Autowired
	EffectRepository effectRepository;
	
	public List<Effect> getAll(){
		return effectRepository.findAll();
	}
	public void saveEffect(Effect effect){
		effectRepository.save(effect);
	}
	public Effect findByEffect(String effect){
		return effectRepository.findByEffect(effect);
	}
}
