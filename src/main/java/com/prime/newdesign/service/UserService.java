package com.prime.newdesign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.prime.newdesign.model.User;
import com.prime.newdesign.repository.UserRepository;
import com.prime.newdesign.util.Constants;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PasswordEncoder bCryptPasswordEncoder;
	
	public User findbyUsername(String username){
		return userRepository.findByUsername(username);
	}
	public void saveUser(User user){
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);
	}
	
	public boolean checkMatch(User user, String oldpw){
		String pw = user.getPassword();
		boolean ismatch = bCryptPasswordEncoder.matches(oldpw, pw);
		return ismatch;
	}
	
	public void deleteUser(User user){
		userRepository.delete(user);
	}
	
	public List<User> getAllUser(){
		return userRepository.findAll();
	}
}
