package com.prime.newdesign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "product")
public class Product {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "size")
	private String size;
	
	@Column(name = "factory")
	private String factory;
	
	@Column(name = "laborcode")
	private String laborcode;
	
	@Column(name = "technology")
	private String technology;
	
	@Column(name = "effect")
	private String effect;
	
	@Column(name = "material")
	private String material;
	
	@Temporal(TemporalType.TIMESTAMP)
    Date selecteddate;
	
	@Temporal(TemporalType.TIMESTAMP)
    Date productiondate;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "url")
	private String url;
	
	public Product() {
		super();
	}

	public int getId() {
		return id;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getLaborcode() {
		return laborcode;
	}

	public void setLaborcode(String laborcode) {
		this.laborcode = laborcode;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public Date getSelecteddate() {
		return selecteddate;
	}

	public void setSelecteddate(Date selecteddate) {
		this.selecteddate = selecteddate;
	}

	public Date getProductiondate() {
		return productiondate;
	}

	public void setProductiondate(Date productiondate) {
		this.productiondate = productiondate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
}
