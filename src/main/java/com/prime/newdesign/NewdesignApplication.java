package com.prime.newdesign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewdesignApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewdesignApplication.class, args);
	}

}
