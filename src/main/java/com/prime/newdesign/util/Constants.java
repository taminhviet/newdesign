package com.prime.newdesign.util;

public class Constants {

	//Role
	public static final int ROLE_ADMIN = 1;
	public static final int ROLE_DESIGNER = 2;
	
	public static final String ADMIN = "ADMIN";
	public static final String DESIGNER = "DESIGNER";
	
	public static final String DOT = ".";
	public static final String COMMA = ",";
	public static final String MINUS = "--";
	public static final String JPEG = "jpeg";
	
	public static final String SINGLE_QUOTE = "'";
	public static final String EMPTY = "";
	public static final String FOLDER_TILES = "tiles";
	
	public static final String DL = "DL";
	public static final String DAI_LOC = "Đại Lộc";
	public static final String PY = "PY";
	public static final String PHO_YEN = "Phổ Yên";
	public static final String YB = "YB";
	public static final String YEN_BINH = "Yên Bình";
	public static final String DV = "DV";
	public static final String DAI_VIET = "Đại Việt";
	public static final String TP = "TP";
	public static final String TIEN_PHONG = "Tiền Phong";
	public static final String VP = "VP";
	public static final String VINH_PHUC = "Vĩnh Phúc";
	
}
