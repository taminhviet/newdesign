jQuery(document).ready(function(){
	$('#tabledesign').DataTable({
		"pageLength": 50,
		"order": [[ 0, "desc" ]]
	});
	$('#tabledistributor').on('click','.btnEditDis', function (e) {
    	var $row = $(this).closest("tr");
    	var $editid = $row.find(".distributorid").text();
    	var $fullname = $row.find(".fullname").text();
    	var $shortname = $row.find(".shortname").text();
    	var $address = $row.find(".address").text();
    	var $email = $row.find(".email").text();
    	var $phone = $row.find(".phone").text();
    	var $sale = $row.find(".sale").text();
    	$("#editid").val($editid.trim());
    	$("#editfullname").val($fullname.trim());
    	$("#editshortname").val($shortname.trim());
    	$("#editaddress").val($address.trim());
    	$("#editemail").val($email.trim());
    	$("#editphone").val($phone.trim());
    	$("div.editsalediv select").val($sale);
    });
	$('.datepicker').datepicker();
	$('.datepickerto').datepicker();
});